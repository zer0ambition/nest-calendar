import { EventDto } from './event.dto';

export class EventListDto {
  events: EventDto[];
}
