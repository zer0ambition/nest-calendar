export class EventDto {
  id!: number;

  name: string;

  description?: string;
}
